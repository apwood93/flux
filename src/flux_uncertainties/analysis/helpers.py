from typing import Optional

import numpy as np
import pandas as pd

from ROOT import TH1D, TH2D
from uuid import uuid4


def get_bin_edges_from_dataframe(df: pd.DataFrame) -> np.ndarray:
    energy_columns = ["E_low", "E_high"]
    if not set(energy_columns).issubset(df.columns):
        raise ValueError(
            "DataFrame does not contain the lower and upper energy bin edges."
        )
    return np.unique(df[energy_columns].to_numpy().flatten())


def calculate_correlation_matrix(covariance_matrix: pd.DataFrame) -> pd.DataFrame:
    """Calculates the correlation matrix for a given covariance matrix."""
    variance = np.sqrt(np.diag(covariance_matrix))
    outer_product = np.outer(variance, variance)
    correlation_matrix = covariance_matrix / outer_product
    correlation_matrix[covariance_matrix == 0] = 0  # Set NaN elements to 0
    return correlation_matrix


def convert_pandas_to_th1(
    series: pd.Series, bin_edges: np.ndarray, hist_title: Optional[str] = None
) -> TH1D:
    if hist_title is None:
        hist_title = str(uuid4())

    th1 = TH1D(hist_title, "", len(bin_edges) - 1, bin_edges)

    for b, val in enumerate(series.values):
        th1.SetBinContent(b + 1, val)

    return th1


def convert_pandas_to_th2(dataframe: pd.DataFrame, hist_title: str) -> TH2D:
    rows = dataframe.index
    columns = dataframe.columns

    xbins = np.arange(0, len(rows) + 1, dtype=float)
    ybins = np.arange(0, len(columns) + 1, dtype=float)
    nbinsx = xbins.shape[0] - 1
    nbinsy = ybins.shape[0] - 1

    th2 = TH2D(hist_title, "", nbinsx, xbins, nbinsy, ybins)

    for ii, (_, row) in enumerate(dataframe.iterrows()):
        for jj, (_, col) in enumerate(row.items()):
            th2.SetBinContent(ii + 1, jj + 1, col)

    it = zip(enumerate(rows), enumerate(columns))

    for (ii, row), (jj, col) in it:
        labelx = f"{row[0]}-{row[1]}-{row[2]}"
        labely = f"{col[0]}-{col[1]}-{col[2]}"
        th2.GetXaxis().SetBinLabel(ii + 1, labelx)
        th2.GetYaxis().SetBinLabel(jj + 1, labely)

    return th2

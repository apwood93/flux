from numpy import arange
from seaborn import heatmap
import matplotlib.pyplot as plt

from flux_uncertainties.visualization.style import neutrino_labels


def plot_covariance(ax, Matrix, cbar_label="covariance", **heatmap_params) -> None:
    params_dict = {
        "ax": ax,
        "cmap": "bwr",
        "vmin": -1,
        "vmax": 1,
    }

    params_dict.update(heatmap_params)

    ax = heatmap(Matrix, **params_dict)

    cbar = ax.collections[0].colorbar

    cbar.set_label(cbar_label, loc="center")

    nrows = Matrix.shape[0]

    axis_labels = [
        neutrino_labels["nue"] + r"$^{fhc}$",
        neutrino_labels["nuebar"] + r"$^{fhc}$",
        neutrino_labels["numu"] + r"$^{fhc}$",
        neutrino_labels["numubar"] + r"$^{fhc}$",
        neutrino_labels["nue"] + r"$^{rhc}$",
        neutrino_labels["nuebar"] + r"$^{rhc}$",
        neutrino_labels["numu"] + r"$^{rhc}$",
        neutrino_labels["numubar"] + r"$^{rhc}$",
    ]

    n_boxes = len(axis_labels)

    nbins = int(nrows / n_boxes)

    divisions = arange(nbins / 2, nrows, step=nbins)

    ax.set_ylim(reversed(ax.get_ylim()))

    ax.set_xticks(divisions)
    ax.set_yticks(divisions)

    ax.set_xticklabels(
        axis_labels,
        rotation=0,
        verticalalignment="top",
    )
    ax.set_yticklabels(axis_labels)

    ax.set_xlabel("")
    ax.set_ylabel("")

    for i in range(1, n_boxes + 1):
        lw = 2 if i == int(n_boxes / 2) else 1
        ax.axvline(x=i * nbins, linewidth=lw, color="k")
        ax.axhline(y=i * nbins, linewidth=lw, color="k")

    plt.tight_layout()

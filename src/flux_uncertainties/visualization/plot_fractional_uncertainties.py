import mplhep
import matplotlib.pyplot as plt

from flux_uncertainties.visualization.style import (
    okabe_ito,
    neutrino_labels,
    xlabel_enu,
)


ppfx_labels = {
    "pCpi": r"$p + C \to \mathrm{\pi}^\pm + X$",
    "pCnu": r"$p + C \to N + X$",
    "pCk": r"$p + C \to K + X$",
    "nCpi": r"$n + C \to \mathrm{\pi}^\pm + X$",
    "mesinc": r"$(\mathrm{\pi}^\pm, K) + A \to (\mathrm{\pi}^\pm, K, N) + X$",
    "nua": r"$N + A \to X$",
    "nuAlFe": r"$N + (Al, Fe)\to X$",
    "attenuation": "attenuation in the target",
    "others": "others",
    "total": "total",
}


def plot_fractional_uncertainties(ax, uncerts, nu, horn_current, **kwargs) -> None:
    total = uncerts["total"]

    labels = [ppfx_labels[key] for key in uncerts if key != "total"]

    hists = [h for k, h in uncerts.items() if k != "total"]

    colors = [color for name, color in okabe_ito.items() if name != "black"]

    histplot_opts = {
        "ax": ax,
        "lw": 2,
        "edges": False,
        "yerr": False,
    }
    histplot_opts.update(kwargs)

    color = colors
    ls = 5 * ["-"] + 4 * ["--"]

    mplhep.histplot(H=hists, label=labels, color=color, ls=ls, **histplot_opts)
    mplhep.histplot(H=total, label="total", color="k", ls="-", **histplot_opts)

    ax.set_xlabel(xlabel_enu)
    ax.set_ylabel(r"Fractional Uncertainty ($\mathrm{\sigma_\phi}$ / $\mathrm{\phi}$)")

    ax.legend(loc="upper right", ncol=2, fontsize=20, columnspacing=0.5)

    ax.set_xlim(0, 6)

    ax.text(
        0.0,
        1.015,
        f"{horn_current} {neutrino_labels[nu]}",
        fontweight="bold",
        fontstyle="italic",
        fontsize=28,
        transform=ax.transAxes,
    )

    plt.tight_layout()

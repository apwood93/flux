import mplhep
import matplotlib.pyplot as plt

from cycler import cycler


okabe_ito = {
    "orange": "#E69F00",
    "skyblue": "#56B4E9",
    "bluishgreen": "#009E73",
    "yellow": "#F0E442",
    "blue": "#0072B2",
    "vermillion": "#D55E00",
    "reddishpurple": "#CC79A7",
    "black": "#000000",
}

neutrino_labels = {
    "nue": r"$\mathrm{\nu_{e}}$",
    "nuebar": r"$\mathrm{\bar{\nu}_{e}}$",
    "numu": r"$\mathrm{\nu_{\mu}}$",
    "numubar": r"$\mathrm{\bar{\nu}_{\mu}}$",
}

xlabel_enu = r"E$_\mathrm{\nu}$ (GeV)"
ylabel_flux = r"$\mathrm{\phi_\nu}$ (m$^{-2}$ GeV$^{-1}$ POT$^{-1}$)"


def apply_style() -> None:
    plt.style.use(mplhep.style.ROOT)

    default_cycler = cycler(linestyle=["-", "--"]) * cycler(color=okabe_ito.values())

    mpl_opts = {
        "axes.prop_cycle": default_cycler,
        "axes.formatter.use_mathtext": True,
        "axes.formatter.limits": [-3, 3],
        "xaxis.labellocation": "center",
        "yaxis.labellocation": "center",
        "font.family": "TeX Gyre Heros",
        "font.size": 32,
        "mathtext.fontset": "stix",
    }

    plt.rcParams.update(mpl_opts)

from functools import reduce
import multiprocessing as mp
from typing import Optional

import pandas as pd
import numpy as np

from .normalize_and_rebin_data import normalize_flux_to_pot
from .dataframes import (
    construct_nominal_dataframe,
    construct_hadron_production_dataframe,
)


class Preprocessor:
    __slots__ = (
        "results_directory",
        "file_dict",
        "bin_edges",
    )

    def __init__(
        self,
        results_directory: str,
        file_dict: dict[str, tuple[str]],
        bin_edges: Optional[list[float]] = None,
        # bin_width: bool = False,
    ) -> None:
        self.results_directory = results_directory
        self.bin_edges = np.asarray(bin_edges)

        if (len(file_dict["fhc"]) == 0) and (len(file_dict["rhc"]) == 0):
            print("Please provide at least one of FHC/RHC file path!")
            return

        # norm_func = partial(self._normalize_and_rebin_helper, bin_width=bin_width)

        jobs = []

        for horn, files in file_dict.items():
            if len(files) == 1:
                job = horn, 15, files[0]
                jobs.append(job)
                continue
            for f in files:
                fsplit = f.rsplit("_", 2)
                run_id = int(fsplit[-2])
                job = horn, run_id, f
                jobs.append(job)

        with mp.Pool(processes=(mp.cpu_count() - 1)) as pool:
            res = pool.starmap(self._normalize_and_rebin_helper, jobs)

        self.file_dict = reduce(lambda a, b: {**a, **b}, res)

    def _normalize_and_rebin_helper(
        self, horn, run_id, f
    ) -> dict[tuple[str, int], str]:
        normed = normalize_flux_to_pot(
            input_file=f,
            output_dir=self.results_directory,
            bin_edges=self.bin_edges,
        )
        return {(horn, run_id): normed}

    @property
    def nominal_flux_df(self) -> pd.DataFrame:
        jobs = ((f, horn, run_id) for (horn, run_id), f in self.file_dict.items())

        with mp.Pool(processes=(mp.cpu_count() - 1)) as pool:
            res = pool.starmap(construct_nominal_dataframe, jobs)

        nominal_flux = pd.concat(res, ignore_index=True)

        return nominal_flux

    @property
    def ppfx_correction_df(self) -> pd.DataFrame:
        jobs = []
        if ("fhc", 15) in self.file_dict:
            jobs.append((self.file_dict[("fhc", 15)], "fhc"))
        if ("rhc", 15) in self.file_dict:
            jobs.append((self.file_dict[("rhc", 15)], "rhc"))

        with mp.Pool(processes=2) as pool:
            res = pool.starmap(construct_hadron_production_dataframe, jobs)

        ppfx_correction = pd.concat(res, ignore_index=True) if len(res) > 1 else res[0]

        return ppfx_correction

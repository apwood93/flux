import os
from pathlib import Path
from typing import Any, Optional

# import ROOT
from ROOT import TFile  # type: ignore

import uproot


class Exporter:
    products_filepath: str

    def __init__(
        self,
        results_directory: str,
        products_filename: Optional[str] = "analysis_products.root",
    ) -> None:
        self.products_filepath = (
            f"{Path(results_directory).expanduser()}/{products_filename}"
        )

    def init_products_file(self) -> None:
        exists = os.path.isfile(self.products_filepath)
        if exists:
            # print("Analysis products file already exists, overwriting...")
            os.remove(self.products_filepath)
        cmd = f"rootmkdir -p {self.products_filepath}:ppfx_output/{{fhc,rhc}}"
        os.system(cmd)
        # print(f"Analysis products being written to {self.products_filepath}")

    def export_ppfx_output(
        self, fhc_file: Optional[str] = None, rhc_file: Optional[str] = None
    ) -> None:
        if fhc_file is None and rhc_file is None:
            print("Please provide at least one of FHC/RHC file path!")
            return
        # print("Writing PPFX output...")
        if fhc_file is not None:
            cmd1 = f"rootcp -r {fhc_file} {self.products_filepath}:ppfx_output/fhc/"
            os.system(cmd1)
        if rhc_file is not None:
            cmd2 = f"rootcp -r {rhc_file} {self.products_filepath}:ppfx_output/rhc/"
            os.system(cmd2)
        # print("PPFX output written to", self.products_filepath)

    def export_product(self, product, key: str) -> None:
        with uproot.update(self.products_filepath) as products:
            products[key] = product

    def export_products_dict(self, products: dict[str, Any]) -> None:
        product_file = TFile(self.products_filepath, "update")
        for key, product in products.items():
            dirs = key.split("/")

            if len(dirs) == 1:
                product_file.WriteObject(product, key)
            else:
                subdirs = "/".join(dirs[:-1])

                if not product_file.Get(subdirs):
                    product_file.mkdir(subdirs)
                d = product_file.Get(subdirs)

                d.WriteObject(product, dirs[-1])

        product_file.Close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        print(f"Done.\nAnalysis products written to {self.products_filepath}")
